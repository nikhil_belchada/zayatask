import os
from django.core.exceptions import ValidationError

def validate_file_extension(value):
    file_path, extension = os.path.splitext(value.name)
    VALID_EXTENSIONS = ['.mp4']
    MAX_VIDEO_SIZE = 75 * 1024 * 1024
    if not extension in VALID_EXTENSIONS:
        raise ValidationError(u'Unsupported file format.')
    if value.size > MAX_VIDEO_SIZE:
        raise ValidationError(u'Max video size is 75mb.')
