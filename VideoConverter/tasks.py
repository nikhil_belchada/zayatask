import os
from celery import task
from VideoConverter.models import Uploads
from boto.s3.key import Key
from .utils import create_connection

@task()
def compress_video_task(uploaded_file_id, resolution=Uploads.LOW_RESOLUTION, audio_frequency=Uploads.LOW):
    resoultion_list = [res[0] for res in Uploads.RESOLUTION_LIST]
    if resolution not in resoultion_list:
        print 'Invalid Resolution.' + 'Allowed resolution: ' + ', '.join(resoultion_list)
        return

    audio_frequency_list = [frequency[0] for frequency in Uploads.AUDIO_FREQUENCY]
    if audio_frequency not in audio_frequency_list:
        print 'Invalid Frequency.' + 'Allowed frequency: ' + ', '.join(audio_frequency)
        return

    file_uploaded = Uploads.objects.get(id=uploaded_file_id)
    filename = file_uploaded.file.name
    print "Establishing connection to S3...."
    try:
        bucket = create_connection()
    except Exception as e:
        print "Falied to connect to S3 using provided Credentials"
        return
    print "Connection Established"
    print "Checking %s file in S3..." % filename
    key = Key(bucket, filename)
    print "File found."
    print "Downloading %s file..." % filename
    download_video(key, filename)
    print "File downloaded successfully"
    print "Compressing Video"
    filename_base, filename_ext = os.path.splitext(filename)
    compressed_file_name = ("%s_compressed" + filename_ext) % filename_base
    try:
        compress_file(filename, compressed_file_name, resolution, audio_frequency)
        print "Video compressed successfully."
        print "Upload compressed file %s to S3.." % compressed_file_name
        key = Key(bucket)
        key.key = compressed_file_name
        key.set_contents_from_filename(compressed_file_name)
        print "file upload complete"
        file_uploaded.compressed_file_url = key.generate_url(expires_in=0, query_auth=False)
        file_uploaded.is_compressed = True
        file_uploaded.save()
    except Exception as e:
        print "Error compressing file"
    if os.path.exists(compressed_file_name):
        os.remove(compressed_file_name)
    os.remove(filename)

def download_video(key, file_path):
    with open(file_path, 'wb'):
        key.get_contents_to_filename(file_path)

def compress_file(filename, output_filename, resolution, audio_frequency):
    import subprocess
    COMMAND = 'ffmpeg -i ' + filename + ' -vf scale=' + resolution + ' -ar ' + audio_frequency + ' -strict -2 ' + output_filename
    subprocess.call(COMMAND, shell=True)
