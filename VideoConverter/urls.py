from django.conf.urls import include, url
from .views import IndexView, UploadView

urlpatterns = [
    url(r'^$', IndexView.as_view(), name='index_page'),
    url(r'^upload$', UploadView.as_view(), name='upload_page'),
]
