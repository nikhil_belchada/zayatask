from django import forms
from VideoConverter.models import Uploads


class UploadForm(forms.ModelForm):
    file = forms.FileField(error_messages={'required': 'File is required'})

    class Meta:
        model = Uploads
        fields = ['file', 'resolution', 'audio_frequency']
