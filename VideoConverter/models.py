import os
from django.utils.timezone import now
from django.db import models
from .validators import validate_file_extension
from django.utils import timezone
# Create your models here.


def upload_handler(instance, filename):
    filename_base, filename_ext = os.path.splitext(filename)
    return '%s%s' % (now().strftime("%Y%m%d%H%M%S"), filename_ext.lower())

class Uploads(models.Model):
    LOW_RESOLUTION = '256:144'
    MEDIUM_RESOLUTION = '426:240'
    LARGE_RESOLUTION = '640:360'
    EX_LARGE_RESOLUTION = '1280:720'
    RESOLUTION_LIST = (
        ('256:144', '144p'),
        ('426:240', '240p'),
        ('640:360', '360p'),
        ('1280:720', '720p'),
    )
    HIGH = '44100'
    MEDIUM = '22050'
    LOW = '11025'
    AUDIO_FREQUENCY = (
        (LOW, 'Low'),
        (MEDIUM , 'Medium'),
        (HIGH, 'High')
    )
    DEFAULT_AUDIO_FREQUENCY = AUDIO_FREQUENCY[1]
    name = models.CharField(max_length=100, blank=True, null=True)
    file = models.FileField(upload_to=upload_handler, validators=[validate_file_extension])
    size = models.FloatField(null=True)
    is_compressed = models.BooleanField(default=False)
    file_url = models.URLField(null=True, blank=True)
    compressed_file_url = models.URLField(null=True, blank=True)
    uploaded = models.DateTimeField(default=timezone.now, blank=True)
    resolution = models.CharField(max_length=7, choices=RESOLUTION_LIST, default=LOW_RESOLUTION)
    audio_frequency = models.CharField(verbose_name='Audio', max_length=5, choices=AUDIO_FREQUENCY, default=LOW)
