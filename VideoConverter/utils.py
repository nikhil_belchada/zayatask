from boto.s3.connection import S3Connection
from django.conf import settings

def create_connection():
    connection = S3Connection(settings.AWS_S3_ACCESS_KEY_ID, settings.AWS_S3_SECRET_ACCESS_KEY)
    bucket = connection.get_bucket(settings.AWS_STORAGE_BUCKET_NAME)
    return bucket