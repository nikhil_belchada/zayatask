from django.shortcuts import render, render_to_response, RequestContext
from django.views.generic.base import View
from .forms import UploadForm
from .models import Uploads
from .tasks import compress_video_task

class IndexView(View):
    def get(self, request):
        uploaded_videos = Uploads.objects.order_by('-uploaded')
        upload_urls = [{
            'name': video.name,
            'url': video.compressed_file_url if video.is_compressed else video.file_url,
            'resolution': video.resolution,
            } for video in uploaded_videos]
        return render(request, 'index.html', {'videos': upload_urls})


class UploadView(View):
    def get(self, request):
        form = UploadForm()
        return render_to_response('uploads/fileUpload.html',
                                  {'form': form},
                                  context_instance=RequestContext(request))

    def post(self, request):
        form = UploadForm(request.POST, request.FILES)
        audio_frequency = request.POST.get('audio_frequency')
        resolution = request.POST.get('resolution')
        if form.is_valid():
            file_object = form.save()
            file_object.size = file_object.file.size
            file_object.name = request.FILES['file'].name
            file_object.file_url = file_object.file.url
            file_object.save()
            compress_video_task.delay(file_object.id, resolution, audio_frequency)
            return render_to_response('uploads/fileUpload.html',
                                  {
                                      'form': form,
                                       'message': 'File Upload Successfully.'
                                   },
                                  context_instance=RequestContext(request))
        return render_to_response('uploads/fileUpload.html',
                                  {'form': form},
                                  context_instance=RequestContext(request))
